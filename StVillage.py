class Buildings:
    
    def __init__(self, street, building_number, district) -> None:
        self.street = street
        self.building_number = building_number
        self.district = district
        
    def __str__(self) -> str:
        return f"Budynek nr {self.building_number} przy ulicy {self.street} w dzielnicy {self.district}"
    

class Residential(Buildings):
    
    def add_residential_building(self, height, floors, usable_area, number_of_flats, rent, max_occupancy):
        self.height = height
        self.floors = floors
        self.usable_area = usable_area
        self.number_of_flats = number_of_flats
        self.rent = rent
        self.max_occupancy = max_occupancy


class Manufacturing(Buildings):
    
    def add_manufacturing_building(self, manufactury_type, manufactury_performance, workers_needed):
        self.manufactury_type = manufactury_type
        self.manufactury_performance = manufactury_performance
        self.workers_needed = workers_needed


class Services(Buildings):

    def add_service_building(self, service_type, workers_needed, working_hours):
        self.service_type = service_type
        self.workers_needed = workers_needed
        self.working_hours = working_hours


class Residents:
    
    def __init__(self, name, age) -> None:
        self.name = name
        self.age = age
        
    def __str__(self) -> str:
        return f"Cześć! Jestem {self.name} ({self.age})!"
    

class People(Residents):
    
    def people_definition(self, birth_year, sex):
        self.birth_year = birth_year
        self.sex = sex
        
    def people_work(self, work_role, salary, department, work_hours):
        self.work_role = work_role
        self.salary = salary
        self.department = department
        self.work_hours = work_hours


class Animals(Residents):
    
    def animal_definition(self, birth_year, sex, animals_species):
        self.birth_year = birth_year
        self.sex = sex
        self.species = animals_species
        
    def animal_work(self, work_role, salary, department, work_hours):
        self.work_role = work_role
        self.salary = salary
        self.department = department
        self.work_hours = work_hours
        

class Creatures(Residents):
    
    def creatures_definition(self, birth_year, sex, creature_species):
        self.birth_year = birth_year
        self.sex = sex
        self.species = creature_species
        
    def creatures_work(self, work_role, salary, department, work_hours):
        self.work_role = work_role
        self.salary = salary
        self.department = department
        self.work_hours = work_hours